#!/usr/bin/env bash

server=$(dig facebook.com|grep SERVER|cut -d" " -f3|cut -d"#" -f1)
name_server=$(cat /etc/resolv.conf|grep nameserver|cut -d" " -f2)

if [[ "$server" == "$name_server" ]];then
	echo "YaY, succsess the dns server is working fine."
else
	echo "make sure to check your /etc/resolv.conf file.
	make sure it points to your dns server."
fi
