# Explanation of this project:

The Vagrant file contains 3 machines
each assigned its own static ip.
- dnsdeb(Debian 10)
configured as DNS server
- dns(Centos7)
configured as DNS server
- dnsc(Centos7)
configured as a client


the deploy.yml is a playbook configured to work with the vagrant file. 

While uploading the machines the deploy.yml installes DNS server and configures the .conf files to work as caching and forwarding server by changing and additing some lines.

The deploy.yml is also managed to work depending on the OS(debian or redhat)

I have added also the dns_check.sh in order to check from the client if the dns server is responding properly.

# Excuting the project

In order for this project to work just run the command
``vagrant up``
and you will have 3 working machines.

Copy the bash script into the client run it and get the confirmetion that the setup is working.
