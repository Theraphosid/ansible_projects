#!/usr/bin/env bash

dist=$(cat /etc/*-release | egrep "^ID=" | cut -d"=" -f2)

echo "Creating installetion for ansible"
sleep 5
if [[ "$dist" == "debian" ]];then
	apt install python3 -y
	apt install python3-pip -y
	pip3 install ansible
	pip3 install pipenv --user
elif [[ "$dist" == "redhat" ]];then
	yum install python3 -y
	yum install python3-pip -y
	pip3 install ansible
	pip3 install pipenv --user
fi
